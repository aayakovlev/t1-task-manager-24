package ru.t1.aayakovlev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractExtendedModel extends AbstractModel {

    @NotNull
    private Date created = new Date();

    @NotNull
    private String description = "";

    @NotNull
    private String name = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
