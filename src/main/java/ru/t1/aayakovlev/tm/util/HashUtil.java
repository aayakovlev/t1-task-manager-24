package ru.t1.aayakovlev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    String SECRET = "12321";

    @NotNull
    Integer ITERATION = 4321;

    @NotNull
    static String salt(@NotNull final String value) {
        @NotNull String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    @NotNull
    static String md5(@NotNull final String value) {
        try {
            @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes(StandardCharsets.UTF_8));
            @NotNull final StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                @NotNull final String hex = Integer.toHexString((b & 0xFF) | 0x100);
                sb.append(hex, 1, 3);
            }
            return sb.toString();
        } catch (@NotNull final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}
